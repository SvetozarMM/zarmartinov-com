$(document).ready(function() {


    $(".form").submit(function() {
        
        $.ajax({
            type: "POST",
            url: "mailer.php",

            data: {
                name: $("#name").val(),
                email: $("#email").val(),
                message: $("#message").val()
            },
            
            success: function() {
                $('#contact-msg').text("Thank you! Your message has been sent.").css('color', '#05c46b');
                $('.form').trigger("reset");
            },
            error: function() {
                $('#contact-msg').text("Oops! Something went wrong. Please try again!").css('color', '#e55039');
                $('.form').trigger("reset");
            }
          });
    });
    
    
    
});
